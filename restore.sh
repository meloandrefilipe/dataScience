#!/bin/bash
if [ ! $1 ];
then
    echo "Please insert date of the backup on command line like this './restore.sh dd_mm_yyyy'"
    exit 1
fi

if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi


backup_date=$1

if [ ! -d "./.backups/arango/"${backup_date} ] || [ ! -d "./.backups/influx/"${backup_date} ]  
then
	echo "There is no backup dated $backup_date on arango or influx folders!"
	exit 1
fi
if [ "$(docker ps -q -f name=influx)" ] && [ "$(docker ps -q -f name=arango)" ]; 
	then
		echo "Restore of arangodb on the way..."
		docker exec -it arango arangorestore --input-directory "/dump/"${backup_date} --server.password ${ARANGODB_ROOT_PASSWORD}
		echo "Arangodb restore done!"
		echo "Restore of influxdb on the way..."
		docker exec -it influx influx -precision rfc3339 -password ${INFLUXDB_ADMIN_PASSWORD} -username ${INFLUXDB_ADMIN_USER} -execute 'DROP DATABASE influxdb'
		docker exec -it influx influxd restore -portable -newdb influxdb /backups/${backup_date}
		echo "Influxdb restore done!"
	else
		echo "Containers NOT running!"
		echo "Put your containers UP mister!"
		exit 1
fi
echo "Have a good day sir :D"
exit 0