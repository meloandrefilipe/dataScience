require("dotenv/config");
const express = require("express");
const app = express();
const fs = require("fs");
const appRoot = require("app-root-path");
const Database = require("arangojs").Database;
const dbConfig = require(appRoot + "/config/db");
const arangodb = new Database(dbConfig.url);
const Moment = require("moment");
const Influx = require("influx");
const influxdb = new Influx.InfluxDB({
	host: process.env.INFLUXDB_URL,
	database: process.env.INFLUXDB_DB,
	username: process.env.INFLUXDB_ADMIN_USER,
	password: process.env.INFLUXDB_ADMIN_PASSWORD,
	precision: process.env.INFLUXDB_PRECISION,
});

arangodb.useDatabase(dbConfig.database);
arangodb.useBasicAuth(dbConfig.username, dbConfig.password);


let NUMBER_OF_NODES = 50;
let NUMBER_OF_SERVICES = 10;




let nodes = [];
let edges = [];
let traces = [];
let services = [];
let errors = [
	"100 Continue",
	"101 Switching Protocols",
	"102 Processing",
	"200 OK",
	"201 Created",
	"202 Accepted",
	"203 Non - authoritative Information",
	"204 No Content",
	"205 Reset Content",
	"206 Partial Content",
	"207 Multi - Status",
	"208 Already Reported",
	"226 IM Used",
	"300 Multiple Choices",
	"301 Moved Permanently",
	"302 Found",
	"303 See Other",
	"304 Not Modified",
	"305 Use Proxy",
	"307 Temporary Redirect",
	"308 Permanent Redirect",
	"400 Bad Request",
	"401 Unauthorized",
	"402 Payment Required",
	"403 Forbidden",
	"404 Not Found",
	"405 Method Not Allowed",
	"406 Not Acceptable",
	"407 Proxy Authentication Required",
	"408 Request Timeout",
	"409 Conflict",
	"410 Gone",
	"411 Length Required",
	"412 Precondition Failed",
	"413 Payload Too Large",
	"414 Request - URI Too Long",
	"415 Unsupported Media Type",
	"416 Requested Range Not Satisfiable",
	"417 Expectation Failed",
	"418 I'm a teapot",
	"421 Misdirected Request",
	"422 Unprocessable Entity",
	"423 Locked",
	"424 Failed Dependency",
	"426 Upgrade Required",
	"428 Precondition Required",
	"429 Too Many Requests",
	"431 Request Header Fields Too Large",
	"444 Connection Closed Without Response",
	"451 Unavailable For Legal Reasons",
	"499 Client Closed Request",
	"500 Internal Server Error",
	"501 Not Implemented",
	"502 Bad Gateway",
	"503 Service Unavailable",
	"504 Gateway Timeout",
	"505 HTTP Version Not Supported",
	"506 Variant Also Negotiates",
	"507 Insufficient Storage",
	"508 Loop Detected",
	"510 Not Extended",
	"511 Network Authentication Required",
	"599 Network Connect Timeout Error"
]
function buildServices() {
	for (let i = 0; i < NUMBER_OF_SERVICES; i++) {
		let service = {
			name: "service-n" + (i + 1),
			color: getRandomColor(),
			id: "",
		}
		services.push(service);
	}
}
function buildNodes() {
	for (let i = 0; i < NUMBER_OF_NODES; i++) {
		let amountEnd = getRandomInt(1, 10);
		let amountStart = getRandomInt(1, 10);
		let service = services[getRandomInt(0, services.length - 1)].id;
		let second = getRandomInt(5, 20);
		let node = {
			name: "instance-n" + (i + 1),
			id: "",
			service: service,
			endDate: "",
			startDate: "",
		}
		if (amountStart >= 5) {
			node.startDate = Moment(new Date()).add(second, "s")
				.utc()
				.format()
		}
		if (amountEnd >= 5) {
			node.endDate = Moment(new Date()).add(getRandomInt(second + 5, second + 15), "s")
				.utc()
				.format()
		}
		nodes.push(node);
	}
}

async function addServices() {
	for (let i = 0; i < services.length; i++) {
		try {
			let info = await arangodb.query(
				`
				INSERT { name: "${services[i].name}", color: "${services[i].color}"} INTO services
				LET inserted = NEW
				RETURN inserted._id
				`
			);
			services[i].id = info._result[0];
		}
		catch (e) {			// declarações para manipular quaisquer exceções
			console.log(e); // passa o objeto de exceção para o manipulador de erro
		}

	};
}


async function addNodes() {
	for (const node of nodes) {
		try {
			let info = await arangodb.query(
				`
				INSERT { name: "${node.name}", service: "${node.service}", startDate:"${node.startDate}", endDate:"${node.endDate}" } INTO nodes
				LET inserted = NEW
				RETURN inserted._id
				`
			);
			node.id = info._result[0];
		}
		catch (e) {			// declarações para manipular quaisquer exceções
			console.log(e); // passa o objeto de exceção para o manipulador de erro
		}

	};
}

async function addNodesServicesEdges() {

	for (const node of nodes) {
		try {
			await arangodb.query(
				`
				INSERT { _from: "${node.service}", _to: "${node.id}" } INTO nodes_service
				`
			);
		}
		catch (e) {			// declarações para manipular quaisquer exceções
			console.log(e); // passa o objeto de exceção para o manipulador de erro
		}

	};
}


function buildEdges() {
	for (const node of nodes) {
		let conns = getRandomInt(2, 5);
		let random = getRandomInt(1, nodes.length - 1);
		for (let i = 0; i < conns; i++) {
			while (nodes[random].name == node.name) {
				random = getRandomInt(1, nodes.length - 1);
			}
			let edge = {
				source: node.id,
				target: nodes[random].id
			}
			edges.push(edge);
		}
	};
}
async function addNodesEdges() {

	for (const edge of edges) {
		try {
			await arangodb.query(
				`
				INSERT { _from: "${edge.source}", _to: "${edge.target}" } INTO edges
				`
			);
		}
		catch (e) {			// declarações para manipular quaisquer exceções
			console.log(e); // passa o objeto de exceção para o manipulador de erro
		}

	};
}

async function buildTraces() {
	for (const edge of edges) {
		let amount = getRandomInt(5, 20);
		for (let i = 0; i < amount; i++) {
			let times = Moment(new Date()).add(getRandomInt(1, 10), "s")
				.utc()
				.format()
			let vals = { source: isInTime(edge.source, times), target: isInTime(edge.target, times) }
			if (vals.source && vals.target) {
				if (getRandomInt(1, 10) >= 5) {
					let trace = {
						time: times,
						logs: {
							from: edge.source,
							to: edge.target,
							code: errors[getRandomInt(0, errors.length - 1)],
							ping: getRandomInt(10, 200),
							requests: getRandomInt(5, 200)
						}
					}
					traces.push(trace);
				} else {
					let trace = {
						time: times,
						logs: {
							from: edge.target,
							to: edge.source,
							code: errors[getRandomInt(0, errors.length - 1)],
							ping: getRandomInt(10, 200),
							requests: getRandomInt(5, 200)
						}
					}
					traces.push(trace);
				}
			}
		}
	}
}

function isInTime(id, time) {
	let val = 0;
	nodes.forEach(node => {
		if (node.id == id) {
			if ((time >= node.startDate && time <= node.endDate) ||
				(time >= node.startDate && node.endDate == "")) {
				val = 1;
			}
		}
	});
	return val;
}

async function addInfluxLogs() {

	for (const trace of traces) {
		try {
			await influxdb.writeMeasurement("traces", [
				{
					tags: { _from: trace.logs.from, _to: trace.logs.to },
					fields: {
						code: trace.logs.code,
						requests: trace.logs.ping,
						ping: trace.logs.requests,
						time: trace.time,
					},
				}
			]);
		}
		catch (e) {			// declarações para manipular quaisquer exceções
			console.log(e); // passa o objeto de exceção para o manipulador de erro
		}
	}

}

async function main() {
	console.log(Moment(new Date())
		.utc()
		.format());
	console.log("Loading data...");
	buildServices();
	await addServices();
	buildNodes();
	await addNodes();
	buildEdges();
	await addNodesServicesEdges();
	await addNodesEdges();
	await buildTraces();
	await addInfluxLogs();
	console.log("Loaded!");
	console.log(Moment(new Date())
		.utc()
		.format());
}
// main();



function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}
function getRandomColor() {
	let color = "#" + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6);
	while (color == "#000000" || color == "#ffffff" || color == "#ff0000" || color == "#00ff00") {
		color = "#" + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6);
	}
	return color;
}

function randDarkColor() {
	var lum = -0.25;
	var hex = String('#' + Math.random().toString(16).slice(2, 8).toUpperCase()).replace(/[^0-9a-f]/gi, '');
	if (hex.length < 6) {
		hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
	}
	var rgb = "#",
		c, i;
	for (i = 0; i < 3; i++) {
		c = parseInt(hex.substr(i * 2, 2), 16);
		c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
		rgb += ("00" + c).substr(c.length);
	}
	return rgb;
}


async function exportJson(data) {
	fs.writeFile("input.json", JSON.stringify(data), function (err) {
		if (err) { console.log(err) } else {
			console.log('complete');
		}
	}
	);
}



app.listen(process.env.DATAMAKER_PORT, () => {
	console.log("Datamaker running!");
});



