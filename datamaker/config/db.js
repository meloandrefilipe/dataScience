require("dotenv/config");

module.exports = {
	url: "http://" + process.env.ARANGODB_URL + ":" + process.env.ARANGODB_PORT,
	database: process.env.ARANGODB_DATABASE,
	username: process.env.ARANGODB_USER,
	password: process.env.ARANGODB_PASSWORD
};
