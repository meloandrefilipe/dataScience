module.exports = {
	publicPath: "",
	configureWebpack: {
		externals: {
			plotly: "Plotly",
			datepicker: "DateRangePicker"
		},
		devServer: {
			compress: true,
			disableHostCheck: true
		}
	}
};
