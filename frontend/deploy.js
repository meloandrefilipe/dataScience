require("dotenv/config");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(bodyParser.json());
app.use(cors());
app.use(express.static("dist"));

app.listen(process.env.VUEJS_PORT, () => {
	console.log("🚀  Server running 🚀 ");
	console.log("PORT: " + process.env.VUEJS_PORT);
});
