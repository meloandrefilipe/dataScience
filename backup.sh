#!/bin/bash
if [ ! "`whoami`" = "root" ]
then
    echo "Please run script as root."
    exit 1
fi

if [ ! $1 ] || [ ! $2 ];
then
    echo "Please insert username and group on command line like this 'sudo ./backup.sh username group'"
    exit 1
fi

if [ $(id -u $1 > /dev/null 2>&1; echo $?) -eq 1 ];
then
	echo "The user $1 dosen't exist in the system!"
	exit 1
fi

if [ ! $(getent group $2) ];
then
	echo "The group $2 dosen't exist in the system!"
	exit 1
fi

if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

folder_name=$(date +'%d_%m_%Y')
username=$1
group=$2 

arango_dir="./.backups/arango/${folder_name}"
influx_dir="./.backups/influx/${folder_name}"


if [ "$(docker ps -q -f name=influx)" ] && [ "$(docker ps -q -f name=arango)" ]; 
	then
		echo "Backup of influx on the way..."
		sudo rm -R ${influx_dir}
		docker exec -it influx influxd backup -portable -database ${INFLUXDB_DB} "/backups/"${folder_name}
		echo "Giving you permissions to read the backup..."
		sudo chown -R ${username}:${group} ${influx_dir}
		echo "Backup of influxdb done!"
		echo "Backup of influx on the way..."
		sudo rm -R ${arango_dir}
		docker exec -it arango arangodump --output-directory "/dump/"${folder_name} --overwrite true --server.password ${ARANGODB_ROOT_PASSWORD}
		echo "Giving you permissions to read the backup..."
		sudo chown -R ${username}:${group} ${arango_dir}
		echo "Backup of arangodb done!"
	else
		echo "Containers NOT running!"
		echo "Put your containers UP mister!"
		exit 1
fi
echo "Have a good day sir :D"
exit 0