require("dotenv/config");
const { makeExecutableSchema } = require("graphql-tools");
const GraphSchema = require("./graphql/schemas/GraphSchema");
const TimelineSchema = require("./graphql/schemas/TimelineSchema");
const QuerySchema = require("./graphql/schemas/QuerySchema");
const { ApolloServer } = require("apollo-server");
const services = require("./graphql/resolvers/services");
const edges = require("./graphql/resolvers/edges");
const timeline = require("./graphql/resolvers/timeline");
const nodes = require("./graphql/resolvers/node");

const schema = makeExecutableSchema({
  typeDefs: [QuerySchema, GraphSchema, TimelineSchema],
  resolvers: [services, edges, timeline, nodes]
});

const server = new ApolloServer({ schema, playground: false });
server.listen(process.env.NODEJS_PORT, () =>
  console.log("[Server Running]")
);
