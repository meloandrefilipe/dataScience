const appRoot = require("app-root-path");
const database = require("arangojs").Database;
const dbConfig = require(appRoot + "/config/db");
const arangodb = new database(dbConfig.url);
const Influx = require("influx");
const d3 = require("d3");
const Moment = require("moment");
const influxdb = new Influx.InfluxDB({
	host: process.env.INFLUXDB_URL,
	database: process.env.INFLUXDB_DB,
	username: process.env.INFLUXDB_ADMIN_USER,
	password: process.env.INFLUXDB_ADMIN_PASSWORD,
	precision: process.env.INFLUXDB_PRECISION,
});

arangodb.useDatabase(dbConfig.database);
arangodb.useBasicAuth(dbConfig.username, dbConfig.password);

const timeline = {
	Query: {
		timeline(_, args) {
			if (args.start && args.end) {
				return getTimelineData(args.start, args.end);
			} else {
				return null;
			}
		},
	},
};

async function getTimelineData(start, end) {
	var timeline = [];
	timeline.start = start;
	timeline.end = end;
	timeline.traces = [];
	const query = 'SELECT * FROM "traces" WHERE time >= \'' + timeline.start + '\' AND time <= \'' + timeline.end + '\'';
	var data = await influxdb.query(query);

	buildTimelineData(data, timeline);
	return timeline;
}
function buildTimelineData(result, timeline) {
	var nest = d3.nest();
	nest.key(item => item.time._nanoISO);
	nest.key(item => item._from);
	var sorted = nest.entries(result);
	var utcOffset = Moment(timeline.start).parseZone().utcOffset();
	sorted.forEach(element => {
		var trace = [];
		trace.time = toTimeZone(element.key, utcOffset);
		trace.logs = [];
		element.values.forEach(t => {
			t.values.forEach(val => {
				var log = [];
				log.from = val._from;
				log.to = val._to;
				log.code = val.code;
				log.ping = val.ping;
				log.requests = val.requests;
				trace.logs.push(log);
			});

		});
		timeline.traces.push(trace);
	});
}
function toTimeZone(time, offset) {
	return Moment(time).utcOffset(offset).format();
}
module.exports = timeline;