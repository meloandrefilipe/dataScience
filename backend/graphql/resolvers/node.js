const appRoot = require("app-root-path");
const database = require("arangojs").Database;
const dbConfig = require(appRoot + "/config/db");
const arangodb = new database(dbConfig.url);
const Influx = require("influx");
const Moment = require("moment");
const influxdb = new Influx.InfluxDB({
	host: process.env.INFLUXDB_URL,
	database: process.env.INFLUXDB_DB,
	username: process.env.INFLUXDB_ADMIN_USER,
	password: process.env.INFLUXDB_ADMIN_PASSWORD,
	precision: process.env.INFLUXDB_PRECISION,
});

arangodb.useDatabase(dbConfig.database);
arangodb.useBasicAuth(dbConfig.username, dbConfig.password);
const resolver = {
	Query: {
		nodes(_, args) {
			if (args.start && args.end && !args.id) {
				return getNodes(args.start, args.end);
			} else if (!args.start && !args.end && args.id) {
				return getNode(args.id);
			} else if (args.start && args.end && args.id) {
				return getNodeWithTimeStamp(args.id, args.start, args.end)
			} else {
				return getAllNodes()
			}
		},
	},
};

async function getNodes(start, end) {
	let nodes;
	var info = await arangodb.query(
		`
		FOR node IN nodes
			Let connections = LENGTH( FOR edge IN 1..1 ANY node._id edges RETURN edge )
		RETURN {node: node, connections: connections}

		`
	);
	nodes = info._result;
	var data = [];
	for (const node of nodes) {

		var service = await getNodeService(node.node._id);
		var edges = await getNodeEdges(node.node._id);
		const query = 'SELECT * FROM "traces" WHERE time >= \'' + start + '\' AND time <= \'' + end + '\' AND _from = \'' + node.node._id + '\'';
		var res = await influxdb.query(query);
		var traces = [];
		for (const trace of res) {

			var traceData = [{
				code: trace.code,
				latency: trace.ping,
				requests: trace.requests,
			}];
			traces.push({
				timestamp: {
					iso: trace.time._nanoISO,
					utc: Moment(trace.time._nanoISO)
						.utc()
						.format(),
				},
				to: trace._to,
				data: traceData
			});
		}
		data.push({
			id: node.node._id,
			name: node.node.name,
			connections: node.connections,
			cx: node.node.cx,
			cy: node.node.cy,
			service: service,
			edges: edges,
			traces: traces,
			start: node.node.startDate,
			end: node.node.endDate
		})
	}
	return data;
}
async function getNodeWithTimeStamp(id, start, end) {
	let nodes;
	var info = await arangodb.query(
		`
		FOR node IN nodes
			FILTER node._id == "${id}"
			Let connections = LENGTH( FOR edge IN 1..1 ANY node._id edges RETURN edge )
		RETURN {node: node, connections: connections}

  		`
	);
	nodes = info._result;
	var data = [];
	for (const node of nodes) {
		var service = await getNodeService(node.node._id);
		var edges = await getNodeEdges(node.node._id);
		const query = 'SELECT * FROM "traces" WHERE time >= \'' + start + '\' AND time <= \'' + end + '\' AND _from = \'' + node.node._id + '\'';
		var res = await influxdb.query(query);
		var traces = [];
		for (const trace of res) {
			var traceData = [{
				code: trace.code,
				latency: trace.ping,
				requests: trace.requests,
			}];
			traces.push({
				timestamp: {
					iso: trace.time._nanoISO,
					utc: Moment(trace.time._nanoISO)
						.utc()
						.format(),
				},
				to: trace._to,
				data: traceData
			});
		}
		data.push({
			id: node.node._id,
			name: node.node.name,
			connections: node.connections,
			cx: node.node.cx,
			cy: node.node.cy,
			service: service,
			edges: edges,
			traces: traces,
			start: node.startDate,
			end: node.endDate
		})
	}
	return data;

}
async function getAllNodes() {
	let nodes;
	var info = await arangodb.query(
		`
		FOR node IN nodes
			Let connections = LENGTH( FOR edge IN 1..1 ANY node._id edges RETURN edge )
		RETURN {node: node, connections: connections}

  		`
	);
	nodes = info._result;
	var data = [];
	for (const node of nodes) {
		var service = await getNodeService(node.node._id);
		var edges = await getNodeEdges(node.node._id);
		const query = 'SELECT * FROM "traces" WHERE _from = \'' + node.node._id + '\'';
		var res = await influxdb.query(query);
		var traces = [];
		for (const trace of res) {
			var traceData = [{
				code: trace.code,
				latency: trace.ping,
				requests: trace.requests,
			}];
			traces.push({
				timestamp: {
					iso: trace.time._nanoISO,
					utc: Moment(trace.time._nanoISO)
						.utc()
						.format(),
				},
				to: trace._to,
				data: traceData
			});
		}
		data.push({
			id: node.node._id,
			name: node.node.name,
			connections: node.connections,
			cx: node.node.cx,
			cy: node.node.cy,
			service: service,
			edges: edges,
			traces: traces,
			start: node.startDate,
			end: node.endDate
		})
	}
	return data;

}
async function getNode(id) {
	let nodes;
	var info = await arangodb.query(
		`
		FOR node IN nodes
			FILTER node._id == "${id}"
			Let connections = LENGTH( FOR edge IN 1..1 ANY node._id edges RETURN edge )
		RETURN {node: node, connections: connections}

  		`
	);
	nodes = info._result;
	var data = [];
	for (const node of nodes) {
		var service = await getNodeService(node.node._id);
		var edges = await getNodeEdges(node.node._id);
		const query = 'SELECT * FROM "traces" WHERE _from = \'' + node.node._id + '\'';
		var res = await influxdb.query(query);

		var traces = [];
		for (const trace of res) {
			var traceData = [{
				code: trace.code,
				latency: trace.ping,
				requests: trace.requests,
			}];
			traces.push({
				timestamp: {
					iso: trace.time._nanoISO,
					utc: Moment(trace.time._nanoISO)
						.utc()
						.format(),
				},
				to: trace._to,
				data: traceData
			});
		}
		data.push({
			id: node.node._id,
			name: node.node.name,
			connections: node.connections,
			cx: node.node.cx,
			cy: node.node.cy,
			service: service,
			edges: edges,
			traces: traces,
			start: node.startDate,
			end: node.endDate
		})
	}
	return data;

}
async function getNodeEdges(id) {
	var result = [];
	var info = await arangodb.query(
		`
		for e in edges
			filter e._from == "${id}"
		return e
  		`
	);
	info._result.forEach(buildResult);

	return result;
	function buildResult(item) {
		result.push({
			id: item._id,
			source: item._from,
			target: item._to,
		});
	}
}
async function getNodeService(id) {
	let result;
	var info = await arangodb.query(
		`
		FOR v IN 1..1 INBOUND "${id}" nodes_service
		RETURN v
  `
	);
	info._result.forEach((element) => {
		result = {
			id: element._id,
			name: element.name,
			color: element.color,
		};
	});
	return result; // só retorna um serviço pois cada intancia tem apenas um serviço
}

module.exports = resolver;