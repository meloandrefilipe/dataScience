const appRoot = require("app-root-path");
const database = require("arangojs").Database;
const dbConfig = require(appRoot + "/config/db");
const arangodb = new database(dbConfig.url);

arangodb.useDatabase(dbConfig.database);
arangodb.useBasicAuth(dbConfig.username, dbConfig.password);


const resolver = {
	Query: {
		services(_, args) {
			return getServices().then((result) => {
				return result;
			});
		},
	},
};
async function getServices() {
	var result = [];
	var services = await arangodb.collection("services").all();
	services._result.forEach(buildResult);

	var connections = await arangodb.collection("nodes_service").all();
	connections._result.forEach(buildConnections);

	return result;

	function buildConnections(item) {
		result.forEach((element) => {
			if (element.id == item._from || element.id == item._to) {
				element.connections.push({
					id: item._id,
					from: item._from,
					to: item._to,
				});
			}
		});
	}

	function buildResult(item) {
		result.push({
			id: item._id,
			name: item.name,
			color: item.color,
			connections: [],
		});
	}
}



module.exports = resolver;