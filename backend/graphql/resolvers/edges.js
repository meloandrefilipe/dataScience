const appRoot = require("app-root-path");
const database = require("arangojs").Database;
const dbConfig = require(appRoot + "/config/db");
const arangodb = new database(dbConfig.url);

arangodb.useDatabase(dbConfig.database);
arangodb.useBasicAuth(dbConfig.username, dbConfig.password);


const resolver = {
	Query: {
		edges(_, args) {
			return getEdges().then((edges) => {
				if (args.from && !args.edge && !args.to) {
					return edges.filter((edge) => edge.from == args.from);
				} else if (args.edge && !args.from && !args.to) {
					return edges.filter((edge) => edge.id == args.edge);
				} else if (args.to && !args.from && !args.edge) {
					return edges.filter((edge) => edge.to == args.to);
				} else if (!args.edge && args.from && args.to) {
					return edges.filter(
						(edge) => edge.to == args.to && edge.from == args.from
					);
				} else if (args.edge && !args.from && args.to) {
					return edges.filter(
						(edge) => edge.to == args.to && edge.id == args.edge
					);
				} else if (args.edge && args.from && !args.to) {
					return edges.filter(
						(edge) => edge.id == args.edge && edge.from == args.from
					);
				} else if (args.edge && args.from && args.to) {
					return edges.filter(
						(edge) =>
							edge.to == args.to &&
							edge.from == args.from &&
							edge.id == args.edge
					);
				} else {
					return edges;
				}
			});
		},
	},
};
async function getEdges() {
	var result = [];
	var info = await arangodb.collection("edges").all();
	info._result.forEach(buildResult);
	return result;

	function buildResult(item) {
		result.push({
			id: item._id,
			source: item._from,
			target: item._to,
		});
	}
}


module.exports = resolver;