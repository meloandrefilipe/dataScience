const timeline = `
	type timeline {
		start: String!
		end: String!
		traces : [trace]!
	}
	type trace {
		time: String!
		logs : [logs]
	}
	type logs {
		from: String!
		to: String!
		code: String
		ping: String
		requests: String
	}
`;

module.exports = timeline;