const query = ` 

type Query {
    nodes (id: String, node: String, start: String, end: String): [node]
    edges (from: String, edge: String, to: String): [edge]
    services (id: String): [service]
    timeline(start: String, end: String): timeline
}

`;



module.exports = query;
