const graph = `
type node {
    id: ID!
    name: String!
    connections: Int!
    cx: Int
    cy: Int
    start: String
    end: String
    service: service
    edges: [edge]!
    traces: [traceNode]
}
type service {
    id: String!
    name: String!
    color: String!
    connections: [node_service]!
}
type nodeData{
    from: String!
    to: String!
    code: String!
    ping: String!
    time: String!
    requests: String!
}
type edge{
    id: String!
    target: String!
    source: String!
}
type node_service{
    id: String!
    to: String!
    from: String!
}
type traceNode{
    timestamp: timestamp
    to: String!
    data: [traceData]
}
type traceData{
    code: String
    latency: String
    requests: String
}
type timestamp{
    iso: String!
    utc: String!
}
`;

module.exports = graph;
