# Pré-Requisitos

* [Docker](https://docs.docker.com/engine/install/)

* [Docker as non-root user](https://docs.docker.com/engine/install/linux-postinstall/)

* [Docker-Compose](https://docs.docker.com/compose/install/)


# Inicialização

Para inicializar o projeto em qualquer um dos seguintes ambientes (produção/desenvolvimento) é utilizado um comando do docker-compose que irá executar as configurações dos ficheiros destinados a cada ambiente.

## Ambiente de desenvolvimento (Localhost)

### Ficheiro: [docker-compose.yml](https://gitlab.com/meloandrefilipe/dataScience/-/blob/master/docker-compose.yml)

Para iniciar o ambiente de desenvolvimento é necessário executar os seguintes comandos:

```bash
docker-compose down && docker-compose up -d --build
```
> No comando acima os containers são desligados (no caso de já estarem a ser executados) em seguida são reconstruídos (--build caso haja algum update no ficheiro docker-compose) e posteriormente são colocados online (up -d).


## Ambiente de Produção (Servidor)


### Ficheiro: [docker-compose-deploy.yml](https://gitlab.com/meloandrefilipe/dataScience/-/blob/master/docker-compose-deploy.yml)

Para iniciar o ambiente de produção é necessário executar os seguintes comandos:

```bash
docker-compose down && docker-compose -f docker-compose-deploy.yml up -d --build
```
> No comando acima os containers são desligados (no caso de já estarem a ser executados) em seguida são reconstruídos (--build caso haja algum update no ficheiro docker-compose-deploy) e posteriormente são colocados online (up -d).

## Aceder a localização do projeto (localhost)

O projeto irá estar disponível no endereço [local](http://localhost).

# Aceder as Bases de Dados

## ArangoDB

O acesso à base de dados arangodb é realizado apenas no ambiente de desenvolvimento através do endereço [Arango](http://localhost/arango).

## InfluxDB

O acesso à base de dados influxdb é realizado através da utilização da linha de comandos, acedendo ao container docker da respetiva instância. O comando de acesso é:

```bash
docker exec -it influx influx -username root -password (PASSWORD) -database influxdb
```

# Credenciais

Todas as credenciais são definidas no ficheiro [env](https://gitlab.com/meloandrefilipe/dataScience/-/blob/master/.env).

# Comandos úteis do docker

```bash
# Para remover todas as imagens

docker-compose down --rmi all

# Para remover todos os volumes

docker-compose down -v 

# Para remover todos os container órfãos

docker-compose down --remove-orphans

# Parar removendo todas as dependências 

docker-compose down --rmi all -v --remove-orphans
```